import { Component, OnInit, EventEmitter, Output } from "@angular/core";

import { Recipe } from "../recipe.model";
import { RecipeService } from "../recipe.service";

@Component({
  selector: "app-recipe-list",
  templateUrl: "./recipe-list.component.html",
  styleUrls: ["./recipe-list.component.css"]
})
export class RecipeListComponent implements OnInit {
  // recipes: Recipe[] = [
  //   new Recipe(
  //     "A Test Recipe",
  //     "This is simply a test",
  //     "https://cdn.pixabay.com/photo/2017/11/05/11/19/recipes-2920066_960_720.jpg"
  //   ),
  //   new Recipe(
  //     "Another Test Recipe",
  //     "This is simply a Another test",
  //     "https://cdn.pixabay.com/photo/2017/11/05/11/19/recipes-2920066_960_720.jpg"
  //   )
  // ];
  recipes: Recipe[]; // iniailly it is undefined
  @Output() recipeWasSelected = new EventEmitter<Recipe>();

  constructor(private recipeService : RecipeService) {}

  ngOnInit() {
    this.recipes = this.recipeService.getRecipes();
  }

  onRecipeSelected(recipe: Recipe) {
    this.recipeWasSelected.emit(recipe);
  }
  
}
