import { Recipe } from './recipe.model';
import { Ingredient } from '../shared/ingredient.model';

export class RecipeService {
    private recipes: Recipe[] = [
        new Recipe(
          "A Test Recipe",
          "This is simply a test",
          "https://cdn.pixabay.com/photo/2017/11/05/11/19/recipes-2920066_960_720.jpg",
          [
            new Ingredient("Meat", 1),
            new Ingredient("Frech Fries",20)
          ]
        ),
        new Recipe(
          "Another Test Recipe",
          "This is simply a Another test",
          "https://cdn.pixabay.com/photo/2017/11/05/11/19/recipes-2920066_960_720.jpg",
          [
            new Ingredient("buns",2),
            new Ingredient("Meat",1)
          ]
        )
      ];
    
    public getRecipes() {
        return this.recipes.slice();// slice is used bcoz this will give new array which is modified recipes array
    }
}